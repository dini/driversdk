// BP8903_M_TestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BP8903_M_Test.h"
#include "BP8903_M_TestDlg.h"

#include <list>
using namespace std;

typedef unsigned char byte;
typedef unsigned int  size_t;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBP8903_M_TestDlg dialog

CBP8903_M_TestDlg::CBP8903_M_TestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBP8903_M_TestDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBP8903_M_TestDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CBP8903_M_TestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBP8903_M_TestDlg)
	DDX_Control(pDX, IDC_COMBO_GATE, m_ComboBoxGate);
	DDX_Control(pDX, IDC_COMBO_BAUD, m_ComboBoxBaud);
	DDX_Control(pDX, IDC_COMBO_PORT, m_ComboBoxPort);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CBP8903_M_TestDlg, CDialog)
	//{{AFX_MSG_MAP(CBP8903_M_TestDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_ID, OnButtonId)
	ON_BN_CLICKED(IDC_BUTTON_IC, OnButtonIc)
	ON_BN_CLICKED(IDC_BUTTON_ISCONNECT, OnButtonIsconnect)
	ON_BN_CLICKED(IDC_BUTTON_CLEAR, OnButtonClear)
	ON_BN_CLICKED(IDC_BUTTON_POWERON, OnButtonPoweron)
	ON_BN_CLICKED(IDC_BUTTON_POWEROFF, OnButtonPoweroff)
	ON_BN_CLICKED(IDC_BUTTON_APDU, OnButtonApdu)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBP8903_M_TestDlg message handlers
/** 
* @brief 获取所有的串口(从注册表中获取) 
* @param [out] _list 获取到的串口号 
* @retval -1 获取过程出现错误 
* @retval 其他 获取到的HID设备数目
*/ 
int EnumComDevice(list<int>& _list)
{	
	int comCount = -1;
	
	long  lReg; 
	HKEY  hKey;
	DWORD  MaxValueLength;
	DWORD  dwValueNumber;
	
	lReg = RegOpenKeyEx(HKEY_LOCAL_MACHINE, "HARDWARE\\DEVICEMAP\\SERIALCOMM", 0, KEY_QUERY_VALUE, &hKey);
	if(lReg != ERROR_SUCCESS)
	{
		return comCount;
	}
	
	lReg = RegQueryInfoKey(hKey, NULL, NULL ,NULL ,NULL, NULL, NULL, 
		&dwValueNumber, &MaxValueLength, NULL, NULL, NULL);
	if(lReg != ERROR_SUCCESS)
	{
		return comCount;
	}
	
	char valBuff[128];
	char comBuff[8];
	comCount = 0;
	
	DWORD cchValueName,dwValueSize = 6;
	for(DWORD i = 0;i < dwValueNumber; ++i)
	{
		cchValueName = MaxValueLength + 1;
		dwValueSize = 6;
		
		memset(valBuff, 0, sizeof(valBuff));
		
		lReg = RegEnumValue(hKey, i, valBuff, &cchValueName, NULL, NULL, NULL, NULL);
		if(lReg != ERROR_SUCCESS && lReg != ERROR_NO_MORE_ITEMS)
			continue;
		
		memset(comBuff, 0, sizeof(comBuff));
		
		lReg = RegQueryValueEx(hKey, valBuff, NULL, NULL, (LPBYTE)comBuff, &dwValueSize);
		if(lReg != ERROR_SUCCESS)
			continue;
		
		lReg = strlen(comBuff);
		if(lReg > 3 && strstr(comBuff, "COM") != NULL)
		{
			++comCount;
			_list.push_back((int)atoi(comBuff + 3));
		}
	}
	
	return comCount;
}

BOOL CBP8903_M_TestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	list<int> comlist;
	EnumComDevice(comlist);	
	
	SetDlgItemText(IDC_EDIT_TIMEOUT, "10000");

	CString str;
	list<int>::iterator itr;
	for(itr = comlist.begin();itr != comlist.end(); ++itr)
	{
		str.Format("COM%d", *itr);
		m_ComboBoxPort.AddString(str);
	}
	if(comlist.size() > 0)
	{
		m_ComboBoxPort.SetCurSel(0);
	}

	m_ComboBoxBaud.AddString("9600");
	m_ComboBoxBaud.AddString("115200");

	m_ComboBoxBaud.SetCurSel(1);
	SetDlgItemText(IDC_EDIT_SEND, "00 A4 04 00 05 A0 00 00 03 33");

	m_ComboBoxGate.AddString("A");
	m_ComboBoxGate.AddString("B");
	m_ComboBoxGate.AddString("C");
	m_ComboBoxGate.AddString("D");
	m_ComboBoxGate.AddString("K");
	m_ComboBoxGate.SetCurSel(0);
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CBP8903_M_TestDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CBP8903_M_TestDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

int __stdcall IDCard_GetInformation(int port, int baud, const char* gate, int timeout, char* info, const char* srcdir, const char* bmpdir);
void CBP8903_M_TestDlg::OnButtonId() 
{
	// TODO: Add your control notification handler code here
	CString str;
	GetDlgItemText(IDC_COMBO_PORT, str);
	int port = atoi(str.GetBuffer(0) + 3);

	CString sTimeout;
	GetDlgItemText(IDC_EDIT_TIMEOUT, sTimeout);
	int timeout = atoi(sTimeout.GetBuffer(0));

	CString sBaud;
	GetDlgItemText(IDC_COMBO_BAUD, sBaud);
	int baud = atoi(sBaud.GetBuffer(0));

	CString sGate;
	GetDlgItemText(IDC_COMBO_GATE, sGate);
	char gate[2] = {0};
	if(sGate.GetLength() > 0)
		gate[0] = sGate[0];

	char info[256] = {0};
	int iRet = IDCard_GetInformation(port, baud, gate, timeout, info, ".\\drivers", ".\\images");
	if(iRet == 0)
	{
		str = info;
	}
	else
	{
		str.Format("返回:<%d>", iRet);
	}
	SetDlgItemText(IDC_EDIT_DATA, str);
}

int __stdcall ICCard_GetCardNumber(int port, int baud, const char* gate, int timeout, char* cardno);
void CBP8903_M_TestDlg::OnButtonIc() 
{
	// TODO: Add your control notification handler code here
	CString str;
	GetDlgItemText(IDC_COMBO_PORT, str);
	int port = atoi(str.GetBuffer(0) + 3);

	CString sTimeout;
	GetDlgItemText(IDC_EDIT_TIMEOUT, sTimeout);
	int timeout = atoi(sTimeout.GetBuffer(0));

	CString sBaud;
	GetDlgItemText(IDC_COMBO_BAUD, sBaud);
	int baud = atoi(sBaud.GetBuffer(0));

	CString sGate;
	GetDlgItemText(IDC_COMBO_GATE, sGate);
	char gate[2] = {0};
	if(sGate.GetLength() > 0)
		gate[0] = sGate[0];

	char info[256] = {0};
	int iRet = ICCard_GetCardNumber(port, baud, gate, timeout, info);
	if(iRet == 0)
	{
		str = info;
	}
	else
	{
		str.Format("返回:<%d>", iRet);
	}
	SetDlgItemText(IDC_EDIT_DATA, str);
}

int __stdcall IsDevConnect(int port, int baud, const char* gate);
void CBP8903_M_TestDlg::OnButtonIsconnect() 
{
	// TODO: Add your control notification handler code here
	CString str;
	GetDlgItemText(IDC_COMBO_PORT, str);
	int port = atoi(str.GetBuffer(0) + 3);

	CString sBaud;
	GetDlgItemText(IDC_COMBO_BAUD, sBaud);
	int baud = atoi(sBaud.GetBuffer(0));

	CString sGate;
	GetDlgItemText(IDC_COMBO_GATE, sGate);
	char gate[2] = {0};
	if(sGate.GetLength() > 0)
		gate[0] = sGate[0];

	BOOL isConnect = IsDevConnect(port, baud, gate);
	if(isConnect)
	{
		str = "设备已连接";
	}
	else
	{
		str = "设备未连接";
	}
	SetDlgItemText(IDC_EDIT_DATA, str);
}

void CBP8903_M_TestDlg::OnButtonClear() 
{
	// TODO: Add your control notification handler code here
	SetDlgItemText(IDC_EDIT_DATA, "");
}

int __stdcall OpenCom(int port, int baud, const char* gate);
int __stdcall PowerOn(byte* pAtr, size_t* pAtrlen);
int __stdcall Apdu(const byte* sApdu, size_t sLen, byte* rApdu, size_t* rLen);
int __stdcall PowerOff();
int __stdcall CloseCom(int baud);

void CBP8903_M_TestDlg::OnButtonPoweron() 
{
	// TODO: Add your control notification handler code here
	CString str;
	GetDlgItemText(IDC_COMBO_PORT, str);
	int port = atoi(str.GetBuffer(0) + 3);
	
	CString sBaud;
	GetDlgItemText(IDC_COMBO_BAUD, sBaud);
	int baud = atoi(sBaud.GetBuffer(0));

	CString sGate;
	GetDlgItemText(IDC_COMBO_GATE, sGate);
	char gate[2] = {0};
	if(sGate.GetLength() > 0)
		gate[0] = sGate[0];

	int iRet = OpenCom(port, baud, gate);
	if(iRet == 0)
	{
		str = "---打开串口成功---";

		byte atr[64] = {0};
		size_t atrlen = 0;

		iRet = PowerOn(atr, &atrlen);
		if(iRet == 0)
		{
			CString atrAscii;
			char tmp[4] = {0};

			for(int i = 0;i < atrlen; ++i)
			{
				sprintf(tmp, "%02X", atr[i]);
				atrAscii += tmp;
			}
			
			str += "\r\nAtr:\r\n";
			str += atrAscii;
		}
		else
		{
			CString powerInfo;
			powerInfo.Format("---卡片上电失败,返回<%d>---", iRet);

			str += "\r\n";
			str += powerInfo;
			str += "\r\n---关闭串口---\r\n";

			CloseCom(baud);
		}
	}
	else
	{
		str.Format("---打开串口失败,返回:<%d>---\r\n", iRet);
	}
	SetDlgItemText(IDC_EDIT_DATA, str);
}

void CBP8903_M_TestDlg::OnButtonPoweroff() 
{
	// TODO: Add your control notification handler code here
	CString sBaud;
	GetDlgItemText(IDC_COMBO_BAUD, sBaud);
	int baud = atoi(sBaud.GetBuffer(0));

	PowerOff();
	CloseCom(baud);
	
	CString str;
	str = "---下电---\r\n---关口---\r\n";

	SetDlgItemText(IDC_EDIT_DATA, str);
}

size_t AsciiToBcd(const char* src, size_t srclen, byte* dst)
{
	if(src == NULL || dst == NULL)
		return 0;
	
	byte tmp;
	int dstlen = srclen / 2;
	int index = 0;
	
	for(int i = 0;i < dstlen; ++i)
	{
		for(int j = 0;j < 2; ++j)
		{
			index  = 2*i + j;
			if(src[index] >= '0' && src[index] <= '9')
				tmp = src[index] - '0';
			else if(src[index] >= 'A' && src[index] <= 'F')
				tmp = src[index] - 'A' + 0x0A;
			else if(src[index] >= 'a' && src[index] <= 'f')
				tmp = src[index] - 'a' + 0x0A;
			else 
				tmp = 0x00;
			
			if(j == 0)
				dst[i] = tmp << 4;
			else 
				dst[i] |= tmp;
		}
	}
	
	return dstlen;
}

void CBP8903_M_TestDlg::OnButtonApdu() 
{
	// TODO: Add your control notification handler code here
	CString sApdu;
	GetDlgItemText(IDC_EDIT_SEND, sApdu);

	sApdu.Remove(' ');
	sApdu.Remove('\r');
	sApdu.Remove('\n');

	if(sApdu.GetLength() < 1)
		return ;

	byte sCmd[512] = {0};
	size_t sLen = 0;

	byte rCmd[512] = {0};
	size_t rLen = 0;

	sLen = AsciiToBcd(sApdu.GetBuffer(0), sApdu.GetLength(), sCmd);

	int iRet = Apdu(sCmd, sLen, rCmd, &rLen);
	if(iRet == 0)
	{
		sApdu = "rApdu:\r\n";
		char tmp[4] = {0};
		for(int i = 0;i < rLen; ++i)
		{
			sprintf(tmp, "%02X", rCmd[i]);
			sApdu += tmp;
		}
	}
	else
	{
		sApdu.Format("---交换APDU失败,返回<%d>---\r\n", iRet);
	}

	SetDlgItemText(IDC_EDIT_DATA, sApdu);
}
