; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CBP8903_M_TestDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "BP8903_M_Test.h"

ClassCount=2
Class1=CBP8903_M_TestApp
Class2=CBP8903_M_TestDlg

ResourceCount=3
Resource2=IDR_MAINFRAME
Resource3=IDD_BP8903_M_TEST_DIALOG

[CLS:CBP8903_M_TestApp]
Type=0
HeaderFile=BP8903_M_Test.h
ImplementationFile=BP8903_M_Test.cpp
Filter=N

[CLS:CBP8903_M_TestDlg]
Type=0
HeaderFile=BP8903_M_TestDlg.h
ImplementationFile=BP8903_M_TestDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDC_COMBO_GATE



[DLG:IDD_BP8903_M_TEST_DIALOG]
Type=1
Class=CBP8903_M_TestDlg
ControlCount=18
Control1=IDC_BUTTON_ID,button,1342242816
Control2=IDC_STATIC,static,1342308352
Control3=IDC_COMBO_PORT,combobox,1344340227
Control4=IDC_EDIT_DATA,edit,1352728644
Control5=IDC_BUTTON_IC,button,1342242816
Control6=IDC_BUTTON_ISCONNECT,button,1342242816
Control7=IDC_STATIC,static,1342308352
Control8=IDC_EDIT_TIMEOUT,edit,1350631552
Control9=IDC_STATIC,static,1342308352
Control10=IDC_COMBO_BAUD,combobox,1344339971
Control11=IDC_BUTTON_CLEAR,button,1342242816
Control12=IDC_STATIC,static,1342308352
Control13=IDC_EDIT_SEND,edit,1350631552
Control14=IDC_BUTTON_APDU,button,1342242816
Control15=IDC_BUTTON_POWERON,button,1342242816
Control16=IDC_BUTTON_POWEROFF,button,1342242816
Control17=IDC_STATIC,static,1342308352
Control18=IDC_COMBO_GATE,combobox,1344340226

