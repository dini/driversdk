// BP8903_M_TestDlg.h : header file
//

#if !defined(AFX_BP8903_M_TESTDLG_H__64F4CE6F_5095_44FA_B74B_EF51068298B1__INCLUDED_)
#define AFX_BP8903_M_TESTDLG_H__64F4CE6F_5095_44FA_B74B_EF51068298B1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CBP8903_M_TestDlg dialog

class CBP8903_M_TestDlg : public CDialog
{
// Construction
public:
	CBP8903_M_TestDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CBP8903_M_TestDlg)
	enum { IDD = IDD_BP8903_M_TEST_DIALOG };
	CComboBox	m_ComboBoxGate;
	CComboBox	m_ComboBoxBaud;
	CComboBox	m_ComboBoxPort;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBP8903_M_TestDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CBP8903_M_TestDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonId();
	afx_msg void OnButtonIc();
	afx_msg void OnButtonIsconnect();
	afx_msg void OnButtonClear();
	afx_msg void OnButtonPoweron();
	afx_msg void OnButtonPoweroff();
	afx_msg void OnButtonApdu();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BP8903_M_TESTDLG_H__64F4CE6F_5095_44FA_B74B_EF51068298B1__INCLUDED_)
