//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by BP8903_M_Test.rc
//
#define IDD_BP8903_M_TEST_DIALOG        102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON_ID                   1000
#define IDC_COMBO_PORT                  1001
#define IDC_EDIT_DATA                   1002
#define IDC_BUTTON_IC                   1003
#define IDC_BUTTON_ISCONNECT            1004
#define IDC_EDIT_TIMEOUT                1005
#define IDC_COMBO_BAUD                  1006
#define IDC_BUTTON_CLEAR                1007
#define IDC_EDIT_SEND                   1008
#define IDC_BUTTON_APDU                 1009
#define IDC_BUTTON_POWERON              1010
#define IDC_BUTTON_POWEROFF             1011
#define IDC_COMBO_GATE                  1012

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
