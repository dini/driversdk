//=========================================================
/**@file 
 * @brief USB三合一驱动 
 */
//---------------------------------------------------------
typedef unsigned char byte;

/// 接触式 
#define BP_CONTACT_SLOT     1
/// 非接触式
#define BP_CONTACTLESS_SLOT 2
/// PSAM卡槽1
#define BP_PSAM1_SLOT       3
/// PSAM卡槽2
#define BP_PSAM2_SLOT       4
//---------------------------------------------------------
/**
 * @brief 给指定卡槽的IC卡上电 
 * @param [in] slot 卡槽号 
 * - 卡槽号 
 *  - 1 接触式
 *  - 2 非接触
 *  - 3 PSAM1
 *  - 4 PSAM2
 * .
 * @param [out] 卡片返回的ATR
 * @param [out] ATR数据的长度
 * @return 0 成功,其他失败
 */
int __stdcall libPowerOn(byte slot, byte* atr, int* atrlen);
/**
 * @brief 给指定卡槽的IC卡上电 
 * @param [in] slot 卡槽号 
 * @param [out] atrBuff 获取的卡片ATR信息ASCII码格式 
 * @return 0 成功,其他失败
 */
int __stdcall libPowerOnAscii(byte slot, char* atrBuff);
/**
 * @brief 交换APDU
 * @param [in] slot 卡槽号 
 * @param [in] sendBcd 发送的APDU 
 * @param [in] sendlen 发送的APDU长度 
 * @param [out] 卡片返回的APDU
 * @param [out] 卡片返回的APDU长度 
 * @return 0 成功,其他失败
 */
int __stdcall libApdu(byte slot, const byte* sendBcd, int sendlen, byte* recvBcd, int* recvlen);
/**
 * @brief 交换APDU 
 * @param [in] slot 卡槽号 
 * @param [in] sendAscii 发送的APDU数据的ASCII码形式 
 * @param [out] recvAscii 接收的APDU数据的ASCII码形式 
 * @return 0 成功,其他失败
 */
int __stdcall libApduAscii(byte slot, const char* sendAscii, char* recvAscii);
/**
 * @brief 给指定卡槽号的IC卡下电 
 * @param [in] slot 卡槽号 
 * @return 0 成功,其他 失败 
 */
int __stdcall libPowerOff(byte slot);
/**
 * @brief 获取上次错误的描述信息 
 * @param [out] msgBuff 获取信息的缓冲区  
 */
void __stdcall GetLastErrMessage(char* msgBuff);
/*
* @brief 获取指定设备上的卡状态 
* @param [in] slot 卡槽号 
* - 卡槽号 
*  - 1 接触式
*  - 2 非接触
*  - 3 PSAM1
*  - 4 PSAM2
* .
* @return 获取到的卡状态 
*/
int __stdcall libGetCardStatus(byte slot);
/**
* @brief 获取IC卡信息 
* @param [in] slot 获取卡号的IC卡卡槽号 
* - slot
*  - 1 接触式 
*  - 2 非接触式 
*  - 其他自动识别接触和非接(非接优先)
* .
* @param [in] taglist 需要获取的标签列表(如:"AEDBCF" "A",字符必须大写) 
* @param [out] tlvInfo 获取到的信息 
* @param [in] aid IC卡应用(为空表示从PSE进行选择) 
* 
* - tlvInfo数据格式 标签(1字符 A ) + 长度(3字符 016 长度为16字符 ) + 数据(N)
*  - A 卡号 
*  - B 姓名 
*  - C 证件类型 
*  - D 证件号 
*  - E 二磁道数据 
*  - F 一磁道数据 
*  - G 电子现金余额 
*  - H 电子现金余额上限 
*  - I 失效日期 
*  - J PAN序列号 
*  - K 单笔交易限额 
*  - L 现金重置阀值 
* .
*/
int __stdcall libIC_GetInfo(byte slot, const char* taglist, char* tlvInfo, const char* aid, byte* poweron_slot);
/**
* @brief 获取IC卡中的磁卡信息 
* @param [out] cardNumber 卡号 
* @param [out] tr2Buff 二磁道等效数据 
*/
int __stdcall libIC_GetCardNumber(char* cardNumber, char* tr2Buff);
//=========================================================