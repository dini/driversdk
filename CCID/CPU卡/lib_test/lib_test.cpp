//=========================================================
//#define USING_STATIC_LIB

#include <Windows.h>
#include <string>
#include <iostream>

#include <winscard.h>
#pragma comment(lib,"winscard.lib")

#ifdef USING_STATIC_LIB
    #include "libFunction.h"
#endif
using namespace std;
//---------------------------------------------------------
#ifndef USING_STATIC_LIB
    typedef unsigned char byte;
    //-----------------------------------------------------
    typedef int (__stdcall *fpPowerOn)(byte slot, byte* atr, int* atrlen);
    typedef int (__stdcall *fpPowerOnAscii)(byte slot, char* atrBuff);
    typedef int (__stdcall *fpApduAscii)(byte slot, const char* sendAscii, char* recvAscii);
    typedef int (__stdcall *fpPowerOff)(byte slot);
    typedef int (__stdcall *fpIC_GetCardNumber)(char* cardNumber, char* tr2Buff);
    typedef int (__stdcall *fpGetCardStatus)(byte slot);
    //-----------------------------------------------------
#endif
//---------------------------------------------------------
int main()
{
    char buff[1024] = {0};
    char tmpBuff[1024] = {0};

#ifndef USING_STATIC_LIB
    HMODULE hDll = NULL;
    hDll = LoadLibrary(".\\ccid_lib.dll");
    if(hDll == NULL)
    {
        cout<<"���ؿ�ʧ��."<<endl;
        cin.get();

        return 1;
    }

    fpPowerOnAscii libPowerOnAscii = NULL;
    fpApduAscii libApduAscii = NULL;
    fpPowerOff libPowerOff = NULL;
    fpIC_GetCardNumber libIC_GetCardNumber = NULL;
    fpGetCardStatus libGetCardStatus = NULL;
    
    libPowerOnAscii = (fpPowerOnAscii)GetProcAddress(hDll, "PowerOnAscii");
    libApduAscii = (fpApduAscii)GetProcAddress(hDll, "ApduAscii");
    libPowerOff = (fpPowerOff)GetProcAddress(hDll, "PowerOff");
    libIC_GetCardNumber = (fpIC_GetCardNumber)GetProcAddress(hDll, "IC_GetCardNumber");
    libGetCardStatus = (fpGetCardStatus)GetProcAddress(hDll, "GetCardStatus");

    if(libPowerOnAscii == NULL || libApduAscii == NULL || libPowerOff == NULL || libIC_GetCardNumber == NULL || libGetCardStatus == NULL)
    {
        FreeLibrary(hDll);
        
        cout<<"���غ���ʧ��."<<endl;
        cin.get();
        
        return 1;
    }
#endif

    char item;
    byte slot;
    string tmp;
    int iRet = 0;
    bool isExit = false;
    int count = 0;
    char c = '0';
    int i = 0;
    int state = 0;

    while(!isExit)
    {
        system("cls");
        cout<<"1.���ÿ��ۺ�"<<endl
            <<"2.�ϵ�"<<endl
            <<"3.����Apdu"<<endl
            <<"4.�µ�"<<endl
            <<"5.��ȡ������"<<endl
            <<"6.ѭ����ȡ���Ų���"<<endl
            <<"7.��ȡ��Ƭ״̬"<<endl
            <<"0.�˳�"<<endl;
        cin>>item;

        switch(item)
        {
        case '1':
            cout<<"�����뿨�ۺ�:"<<endl;
            cin>>tmp;
            slot = (byte)atoi(tmp.c_str());
            break;
        case '2':
            iRet = libPowerOnAscii(slot, buff);
            cout<<"����:<"<<iRet<<">\n";
            
            if(iRet == 0)
            {
                cout<<"ATR:"<<buff<<endl;
            }
            break;
        case '3':
            cout<<"����Apdu:"<<endl;
            cin>>tmp;

            iRet = libApduAscii(slot, tmp.c_str(), buff);
            cout<<"����:<"<<iRet<<">\n";
            if(iRet == 0)
            {
                cout<<"Recv:"<<buff<<endl;
            }
            break;
        case '4':
            libPowerOff(slot);
            break;
        case '5':
            iRet = libIC_GetCardNumber(buff, tmpBuff);
            cout<<"����:"<<iRet<<endl;
            if(iRet == 0)
            {
                cout<<"����:"<<buff<<endl
                    <<"���ŵ�����:"<<tmpBuff<<endl;
            }
            break;
        case '6':
            count = 0;
            c = '0';
            while(c != 'e')
            {
                for(i = 0;i < 200; ++i)
                {
                    iRet = libIC_GetCardNumber(buff, tmpBuff);
                    cout<<"����:"<<iRet<<endl;
                    
                    Sleep(20);
                    ++count;
                    cout<<count<<endl;
                }
                c = (char)cin.get();
            }
            break;
        case '7':
            state = libGetCardStatus(slot);
            cout<<state<<endl;

            {
                if(state & SCARD_STATE_PRESENT)
                {
                    cout<<"SCARD_STATE_PRESENT"<<endl;
                }
            }
            break;
        default:
            isExit = true;
            break;
        }
        system("pause");
    }

    /*
    FreeLibrary(hDll);
    */

    return 0;
}
//=========================================================